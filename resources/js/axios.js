import baseAxios from "axios";

const tokenItem = localStorage.getItem('user_token');

const axios = baseAxios.create({
  baseURL: process.env.NODE_ENV === 'development' ? 'http://127.0.0.1:8000/' : '/',
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': tokenItem || undefined,
  }
});

export default axios;
